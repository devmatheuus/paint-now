const cansCalculate = area => {
  let litersOfPaint = Math.ceil(area / 5);

  const usedCans = {
    liters: +litersOfPaint,
    biggestCan: 0,
    mediumCan: 0,
    smallCan: 0,
    smallestCan: 0
  };

  for (let index = 0; index <= +litersOfPaint; index++) {
    if (Math.ceil(litersOfPaint - 18 >= 0)) {
      usedCans.biggestCan++;
      Math.ceil((litersOfPaint -= 18));
    }
    if (Math.ceil(litersOfPaint - 3.6 >= 0)) {
      usedCans.mediumCan++;
      Math.ceil((litersOfPaint -= 3.6));
    }
    if (Math.ceil(litersOfPaint - 2.5 >= 0)) {
      usedCans.smallCan++;
      Math.ceil((litersOfPaint -= 2.5));
    }
    if (Math.ceil(litersOfPaint - 0.5 >= 0)) {
      usedCans.smallestCan++;
      Math.ceil((litersOfPaint -= 0.5));
    }
  }
  return usedCans;
};

export default cansCalculate;
