import { toast } from 'react-toastify';
import { maxWallSizeArea, minWallSizeArea } from './default';

const areaValidation = area => {
  if (area <= 0) {
    toast.error('Area deve ser maior que 0m²');
    return false;
  }

  if (area > maxWallSizeArea || area < minWallSizeArea) {
    toast.error('Area deve ser estar entre 1m² e 50m²');
    return false;
  }

  return true;
};

export default areaValidation;
