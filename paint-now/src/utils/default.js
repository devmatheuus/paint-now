export const quantityOfWalls = 4;

export const maxWallSizeArea = 50;

export const minWallSizeArea = 1;

export const literOfPaint = 5;

export const windowWidth = 2.0;

export const windowHeight = 1.2;

export const windowArea = windowWidth * windowHeight;

export const doorWidth = 0.8;

export const doorHeight = 1.9;

export const doorArea = doorWidth * doorHeight;

export const minHeightWallWithDoor = 1.9 + 0.3;
