import { toast } from 'react-toastify';
import { doorArea } from './default';

const doorValidation = (door, area) => {
  const doorCalculate = doorArea * ++door;
  const areaCalculate = (area * 50) / 100;

  if (doorCalculate <= areaCalculate) {
    return true;
  } else {
    toast.error(
      'O total de área da(s) porta(s) deve ser no máximo 50% da área de parede'
    );
    return false;
  }
};

export default doorValidation;
