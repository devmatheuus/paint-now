import { toast } from 'react-toastify';
import { windowArea } from './default';

const windowValidation = (window, area) => {
  const windowCalculate = windowArea * ++window;
  const areaCalculate = (area * 50) / 100;

  if (windowCalculate <= areaCalculate) {
    return true;
  } else {
    toast.error(
      'O total de área da(s) janela(s)deve ser no máximo 50% da área de parede'
    );
    return false;
  }
};

export default windowValidation;
