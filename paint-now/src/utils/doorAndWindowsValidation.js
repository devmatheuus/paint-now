import { toast } from 'react-toastify';
import { doorArea, windowArea } from './default';

const doorAndWindowsValidation = (doors, windows, area) => {
  const windowCalculate = windowArea * ++windows;
  const doorCalculate = doorArea * ++doors;
  const areaCalculate = (area * 50) / 100;

  if (windowCalculate + doorCalculate <= areaCalculate) {
    return true;
  } else {
    toast.error('Area deve ser maior que 0m²');
    return false;
  }
};

export default doorAndWindowsValidation;
