const totalLiters = liters => {
  liters++;
  const usedCans = {
    id: 1,
    liter: liters,
    biggestCan: 0,
    mediumCan: 0,
    smallCan: 0,
    smallestCan: 0
  };

  for (let index = 0; index <= liters; index++) {
    if (Math.ceil(liters - 18 >= 0)) {
      usedCans.biggestCan++;
      Math.floor((liters -= 18));
    }
    if (Math.ceil(liters - 3.6 >= 0)) {
      usedCans.mediumCan++;
      Math.floor((liters -= 3.6));
    }
    if (Math.ceil(liters - 2.5 >= 0)) {
      usedCans.smallCan++;
      Math.floor((liters -= 2.5));
    }
    if (Math.ceil(liters - 0.5 >= 0)) {
      usedCans.smallestCan++;
      Math.floor((liters -= 0.5));
    }
  }
  return [usedCans];
};

export default totalLiters;
