import { toast } from 'react-toastify';
import { minHeightWallWithDoor } from './default';

const heightValidation = height => {
  if (height < minHeightWallWithDoor) {
    toast.error(
      'A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.'
    );
    return false;
  }

  return true;
};

export default heightValidation;
