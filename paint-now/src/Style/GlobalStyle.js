import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  
    font-family: 'Inter', sans-serif ;
  }
  
  html{
    font-size: 62.5%;
  }
  
  html, body, #root {
    height: 100%;
  }

  *, button, input {
    border: 0;
    outline: 0;
  }

  :root{
    --pink: #FF577F;
    --pink-focus: #FF427F;
    --black-scale: #212529;
    --gray-focus: #5b6166;
    --dark-gray: #212529;
    --white-gray: #868E96;
    --white-gray-focus: #e9ecef;
    --white: #F8F9FA;
    --negative-feedback: #E83F5B;
    --red:rgb(205, 0, 0);
    --shadow-bgmodal: rgba(0, 0, 0, 0.8);
    --white: #ffffff;
    --cream: #f8f9fa;

  } 

  body{    
    color: var(--pink);

    display: flex;
    justify-content: center;

    h1,h2,h3,h4, h5,h6{
      font-weight: 700;
    }

    button{
      cursor: pointer;
    }

    ul,ol,li{
      list-style: none;
    }

    a{
      text-decoration: none;
    }
  }
`;

export default GlobalStyle;
