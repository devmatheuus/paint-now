import { ResultProvider } from './Result';

const Providers = ({ children }) => <ResultProvider>{children}</ResultProvider>;

export default Providers;
