import { useState, createContext, useContext } from 'react';

export const ResultContext = createContext({});

export const ResultProvider = ({ children }) => {
  const [result, setResult] = useState([]);

  return (
    <ResultContext.Provider value={{ result, setResult }}>
      {children}
    </ResultContext.Provider>
  );
};

export const useResult = () => useContext(ResultContext);
