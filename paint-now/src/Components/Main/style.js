import styled from 'styled-components';

const StyledMain = styled.div`
  width: 100%;
  height: 80.5vh;

  display: flex;
  justify-content: center;
  align-items: flex-start;
  justify-content: space-evenly;

  margin-top: 3.9rem;

  @media (max-width: 630px) {
    flex-direction: column;
    align-items: center;

    height: 100vh;

    margin-top: 1rem;
  }
`;

export default StyledMain;
