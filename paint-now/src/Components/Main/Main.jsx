import StyledMain from './style';

const Main = ({ children }) => (
  <StyledMain className="main">{children}</StyledMain>
);

export default Main;
