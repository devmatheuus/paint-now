import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: center;

  padding: 1%;

  gap: 1rem;
`;

export const Main = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  width: 100vw;
  height: 100%;

  > button {
    width: 30%;
    margin-top: 25px;
  }
`;
