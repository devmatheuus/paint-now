import Button from '../Button';
import { Container, Main } from './style';
import FirstWall from '../Cards/wall1';
import SecondWall from '../Cards/wall2';
import ThirdWall from '../Cards/wall3';
import FourthWall from '../Cards/wall4';
import ModalResult from '../ModalResult';

import { useState } from 'react';

const FormContainer = () => {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      {showModal ? (
        <ModalResult setShowModal={setShowModal} />
      ) : (
        <Main>
          <Container>
            <FirstWall />
            <SecondWall />
            <ThirdWall />
            <FourthWall />
          </Container>
          <Button bgSchema="pink" onClick={() => setShowModal(true)}>
            Conferir resultado
          </Button>
        </Main>
      )}
    </>
  );
};
export default FormContainer;
