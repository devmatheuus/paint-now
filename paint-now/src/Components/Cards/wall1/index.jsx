import React from 'react';

import { useForm, useNumber } from 'lx-react-form';
import { useResult } from '../../../Providers/Result';
import { toast } from 'react-toastify';

import areaValidation from '../../../utils/areaValidation';
import cansCalculate from '../../../utils/cansCalculate';
import doorAndWindowsValidation from '../../../utils/doorAndWindowsValidation';
import doorValidation from '../../../utils/doorValidation';
import heightValidation from '../../../utils/heightValidation';
import windowValidation from '../../../utils/windowValidation';
import { doorArea, windowArea } from '../../../utils/default';

import { StyledContainer } from '../style';
import Input from '../../Inputs';
import Button from '../../Button';

const FirstWall = () => {
  const { result, setResult } = useResult();

  const handleSummit = data => {
    const { wallHeight, wallWidth, door, window } = data;
    const area = wallHeight * wallWidth;

    const areaValidate = areaValidation(area);
    const cansCalculated = cansCalculate(area);

    let heightValidate = false;
    let doorAndWindowsValidate = false;
    let doorValidate = false;
    let windowValidate = false;

    if (door && window > 0) {
      doorAndWindowsValidate = doorAndWindowsValidation(door, window, area);
    }

    if (door > 0) {
      doorValidate = doorValidation(door, area);
      heightValidate = heightValidation(wallHeight);
    }

    if (window > 0) {
      windowValidate = windowValidation(window, area);
    }

    const results = {
      id: 1,
      areaValidate,
      cansCalculated,
      doorAndWindowsValidate,
      doorValidate,
      heightValidate,
      windowValidate
    };

    const ids = result.filter(({ id }) => id).map(item => +item.id);

    if (!ids.includes(results.id)) {
      if (results.areaValidate === true) {
        setResult([...result, results]);
        toast.success('Informações enviadas com sucesso!');
      }
    } else {
      toast.error('Não é possível enviar duas vezes a mesma informação!');
    }
  };

  const wallHeight = useNumber({
    name: 'wallHeight',
    min: '1',
    max: '50',
    optional: false,
    errorText: 'A area da parede deve ser maior que 0m² e menor que 50²'
  });

  const wallWidth = useNumber({
    name: 'wallWidth',
    min: '1',
    max: '50',
    optional: false,
    errorText: 'A area da parede deve ser maior que 0m² e menor que 50m²'
  });

  const window = useNumber({
    name: 'window',
    optional: true,
    errorText:
      'O total de área da janela deve ser no máximo 50% da área de parede'
  });

  const door = useNumber({
    name: 'door',
    optional: true,
    errorText:
      'O total de área da porta deve ser no máximo 50% da área de parede'
  });

  const form = useForm({
    formFields: [wallHeight, wallWidth, window, door],
    submitCallback: formData => {
      handleSummit(formData);
    }
  });

  return (
    <StyledContainer>
      <form className="form" onSubmit={form.handleSubmit}>
        <label>Medidas da primeira parede</label>

        <div
          className="containerCentral"
          style={{
            border: wallHeight.error && '1px solid var(--negative-feedback)'
          }}
        >
          <Input size="m" placeholder="Altura" {...wallHeight.inputProps} />
        </div>

        {wallHeight.error && (
          <p style={{ color: 'var(--negative-feedback)' }}>
            {wallHeight.error}
          </p>
        )}

        <div
          className="containerCentral"
          style={{
            border: wallWidth.error && '1px solid var(--negative-feedback)'
          }}
        >
          <Input
            size="m"
            placeholder="Largura"
            {...wallWidth.inputProps}
            style={{ borderColor: wallWidth.error && 'red' }}
          />
        </div>

        {wallWidth.error && (
          <p style={{ color: 'var(--negative-feedback)' }}>{wallWidth.error}</p>
        )}

        <label>Quantidade de janelas e portas</label>
        <div
          className="containerCentral"
          style={{
            border: window.error && '1px solid var(--negative-feedback)'
          }}
        >
          <Input
            size={`${windowArea} m²`}
            placeholder="Janelas"
            {...window.inputProps}
            style={{ borderColor: window.error && 'red' }}
          />
        </div>

        {window.error && (
          <p style={{ color: 'var(--negative-feedback)' }}>{window.error}</p>
        )}

        <div
          className="containerCentral"
          style={{
            border: door.error && '1px solid var(--negative-feedback)'
          }}
        >
          <Input
            size={`${doorArea} m²`}
            placeholder="Portas"
            {...door.inputProps}
            style={{ borderColor: door.error && 'red' }}
          />
        </div>

        {door.error && (
          <p style={{ color: 'var(--negative-feedback)' }}>{door.error}</p>
        )}

        <Button type="submit" bgSchema="pink" onClick={() => handleSummit}>
          Enviar dados
        </Button>
      </form>
    </StyledContainer>
  );
};

export default FirstWall;
