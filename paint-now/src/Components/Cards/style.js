import styled from 'styled-components';

export const StyledContainer = styled.div`
  .form {
    width: 100%;
    min-width: 200px;
    padding: 1.8rem 2.4rem 1rem 1.6rem;

    display: flex;
    flex-direction: column;
    flex-wrap: wrap;

    border: 1px solid var(--white-gray-focus);
    border-radius: 4px;

    gap: 1rem;

    label {
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 10px;

      color: var(--black-scale);
    }

    input[type='text'] {
      width: 100%;
      height: 5.2rem;
      padding: 0px 16px;

      background: var(--cream);
      border: 2px solid var(--cream);

      border-radius: 8px;
      margin-top: 1.2rem;
    }

    input[type='text']::placeholder {
      width: 19rem;
      height: 2.4rem;

      font-weight: 400;
      font-size: 1.4rem;

      color: var(--white-gray);
    }

    small {
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 20px;

      color: var(--white-gray);
    }

    .containerCentral {
      width: 100%;
      display: flex;

      justify-content: space-around;
    }

    .containerCentral .containerValor {
      display: flex;
      align-items: center;

      width: 15rem;
      padding: 0px 5px;

      gap: 10px;

      background: var(--cream);

      border-radius: 8px;
    }

    .containerCentral .containerValor input {
      width: 100%;
      height: 5.2rem;
      padding: 0px 5px;

      background: var(--cream);
    }
    .containerCentral .containerValor p {
      font-weight: 400;
      font-size: 1.4rem;
      line-height: 26px;

      color: #5b6166;

      width: 21px;
      height: 26px;
    }
    .containerOpcoes {
      display: flex;
      justify-content: center;

      background: var(--cream);

      width: 50%;
      margin-left: 2rem;
      border-radius: 8px;
    }

    .containerOpcoes select {
      width: 94%;
      height: 5.2rem;
      padding: 0px 16px;

      background: var(--cream);
    }

    @media (max-width: 1050px) {
      .formContainer {
        width: 30rem;
      }
    }

    @media (max-width: 630px) {
      .formContainer {
        height: 30rem;
        width: 100%;
      }
      input[type='text'] {
        width: 90%;
        margin-left: 5%;
      }

      small,
      label {
        margin-left: 6%;
      }
    }
  }
`;
