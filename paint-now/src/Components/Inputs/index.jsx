import ValueContainer from './style';

const Input = ({ size, placeholder, ...rest }) => {
  return (
    <ValueContainer>
      <input name="valor" type="number" placeholder={placeholder} {...rest} />
      <p>{size}</p>
    </ValueContainer>
  );
};

export default Input;
