import styled from 'styled-components';

const ValueContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;

  padding: 0px 5px;
  gap: 10px;

  background: var(--cream);

  border-radius: 8px;

  input {
    width: 70%;
    padding: 0px 5px;
    height: 5.2rem;

    background: var(--cream);
  }

  p {
    font-weight: 400;
    font-size: 1.1rem;
    line-height: 26px;
    text-align: end;

    color: var(--gray-focus);

    width: 27%;
    height: 26px;
  }
`;

export default ValueContainer;
