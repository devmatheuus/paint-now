import StyledModalResult from './style';
import { useResult } from '../../Providers/Result';
import Button from '../../Components/Button';

import { GiPaintBucket } from 'react-icons/gi';
import { useEffect, useRef } from 'react';
import totalLiters from '../../utils/totalLiters';

const ModalResult = ({ setShowModal }) => {
  const { result } = useResult();
  const modalRef = useRef();
  const totalOfLiters = [];
  const arrResume = [];

  useEffect(() => {
    const handleOutClick = e =>
      !modalRef.current?.contains(e.target) && setShowModal(false);

    document.addEventListener('mousedown', handleOutClick);

    return () => document.removeEventListener('mousedown', handleOutClick);
  }, []);

  const totalResume = () => {
    return result.forEach(item =>
      totalOfLiters.push(Math.ceil(item.cansCalculated.liters))
    );
  };

  const totalBiggestCan = () => {
    return result.forEach(item =>
      arrResume.push(Math.ceil(item.cansCalculated.biggestCan * 18))
    );
  };

  const totalMediumCan = () => {
    return result.forEach(item =>
      arrResume.push(Math.ceil(item.cansCalculated.mediumCan * 3.6))
    );
  };
  const totalSmallCan = () => {
    return result.forEach(item =>
      arrResume.push(Math.ceil(item.cansCalculated.smallCan * 2.5))
    );
  };
  const totalSmallestCan = () => {
    return result.forEach(item =>
      arrResume.push(Math.ceil(item.cansCalculated.smallestCan * 0.5))
    );
  };
  totalBiggestCan();
  totalMediumCan();
  totalSmallCan();
  totalSmallestCan();

  totalResume();

  const totalLiter = totalOfLiters.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    0
  );

  const resume = totalLiters(
    arrResume.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
      0
    )
  );

  return (
    <StyledModalResult>
      <div ref={modalRef}>
        <Button onClick={() => setShowModal(false)}>X</Button>
        <h1>Resumo Final</h1>
        {result.length > 0 ? (
          <>
            <ul>
              {result?.map(item => (
                <li key={item?.id}>
                  <GiPaintBucket size={25} />
                  Para pintar a parede {item?.id}, serão necessários:{' '}
                  <strong style={{ textDecoration: 'underline' }}>
                    {item?.cansCalculated?.liters} litros
                  </strong>{' '}
                </li>
              ))}
              <p style={{ fontSize: '2rem', fontWeight: 'bolder' }}>
                Total: {totalLiter} litros
              </p>
            </ul>
            <h2>
              {resume?.map(item => (
                <small key={item.id}>
                  Serão necessárias aproximadamente:{' '}
                  <strong style={{ textDecoration: 'underline' }}>
                    {item?.biggestCan}
                  </strong>{' '}
                  lata(s) de 18 litros,{' '}
                  <strong style={{ textDecoration: 'underline' }}>
                    {item?.mediumCan}
                  </strong>{' '}
                  lata(s) de 3.6 litros,{' '}
                  <strong style={{ textDecoration: 'underline' }}>
                    {item?.smallCan}
                  </strong>{' '}
                  lata(s) de 2.5 litros e{' '}
                  <strong style={{ textDecoration: 'underline' }}>
                    {item?.smallestCan}
                  </strong>{' '}
                  lata(s) de 0.5 litros
                </small>
              ))}
            </h2>
          </>
        ) : (
          <h3 style={{ fontSize: '2rem' }}>
            Nenhuma parede foi selecionada para pintura.
          </h3>
        )}
      </div>
    </StyledModalResult>
  );
};

export default ModalResult;
