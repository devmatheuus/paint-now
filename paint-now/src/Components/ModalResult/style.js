import styled from 'styled-components';

const StyledModalResult = styled.div`
  width: 100vw;
  height: 100vh;

  position: absolute;
  top: 0;

  background: var(--shadow-bgmodal);

  display: flex;
  justify-content: center;
  align-items: center;

  div {
    position: relative;

    width: 70%;
    height: 80%;

    border-radius: 10px;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;

    gap: 2rem;

    background: var(--cream);

    overflow-y: auto;

    button {
      position: absolute;
      top: 0;
      right: 0;

      background: transparent;
      padding: 1rem;

      :hover {
        background: transparent;
      }
    }

    h1,
    h2 {
      border-radius: 10px 10px 0 0;

      width: 100%;
      padding: 1.5rem;

      background: var(--pink);
      color: var(--white);

      text-align: center;

      align-self: flex-start;
    }

    ul {
      display: flex;
      flex-wrap: wrap;
      flex-direction: column;
      justify-content: center;
      align-items: center;

      gap: 1rem;

      li {
        font-size: 1.4rem;
        font-weight: bold;

        border-bottom: 1px solid var(--pink);

        width: 100%;
        padding: 2rem;

        svg {
          transform: translateY(7px);

          padding: 0 5px;
        }
      }
    }

    h2 {
      border-radius: 0 0 10px 10px;

      align-self: flex-end;
    }
  }
`;

export default StyledModalResult;
