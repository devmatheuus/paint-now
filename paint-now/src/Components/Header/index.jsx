import StyledHeader from './style';
import Button from '../Button/index';
import { useHistory } from 'react-router-dom';

const Header = () => {
  const history = useHistory();

  return (
    <StyledHeader>
      <div>
        <h1>Paint</h1>
        <h1>Now</h1>
      </div>
      <Button bgSchema="pink" onClick={() => history.push('/')}>
        Sair
      </Button>
    </StyledHeader>
  );
};

export default Header;
