import styled from 'styled-components';

const StyledHeader = styled.header`
  width: 100%;
  max-width: 1400px;
  margin: 0 auto;
  padding: 1.5rem 0;

  display: flex;
  justify-content: space-around;
  align-items: center;

  background: var(--cream);
  box-shadow: 0px 4px 32px -12px var(--shadow-delete-modal);

  div {
    color: var(--black-scale);

    display: flex;

    font-size: 1.2rem;
    font-weight: 900;
    font-size: 1.5rem;

    width: 62%;

    h1 {
      font-weight: 900;
    }

    h1:first-child {
      color: var(--pink);

      margin-right: 1rem;
    }
  }
  button {
    width: 10%;
  }
`;

export default StyledHeader;
