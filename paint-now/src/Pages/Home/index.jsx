import Button from '../../Components/Button';
import { Container } from '../../Components/Container/style';
import { ImageContainer, TextContainer } from './style';

import homeImage from '../../assets/artist-animate.svg';

import { useHistory } from 'react-router-dom';

const Home = () => {
  const history = useHistory();

  return (
    <Container>
      <TextContainer>
        <h1>Chega de desperdícios, você acabou de encontrar o lugar certo </h1>
        <Button bgSchema="pink" onClick={() => history.push('/dashboard')}>
          Iniciar
        </Button>
      </TextContainer>
      <ImageContainer>
        <img src={homeImage} alt="Artista" />
      </ImageContainer>
    </Container>
  );
};

export default Home;
