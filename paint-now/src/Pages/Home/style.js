import styled from 'styled-components';

export const TextContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: flex-start;

  width: 50%;

  gap: 2rem;

  h1 {
    font-size: 3.5rem;
    font-weight: bold;

    width: 250px;
  }

  button {
    width: 40%;
  }

  @media (max-width: 768px) {
    h1 {
      font-size: 2.5rem;

      width: 190px;
    }
  }

  @media (max-width: 500px) {
    width: 100%;

    button {
      width: 80%;
    }
  }

  @media (max-width: 300px) {
    h1 {
      font-size: 2.5rem;
      width: 50%;
    }
  }
`;

export const ImageContainer = styled.div`
  width: 50%;
  height: 100%;

  display: flex;
  align-items: center;
  justify-content: center;

  img {
    width: 100%;
    height: 100%;

    object-fit: contain;
  }

  @media (max-width: 500px) {
    display: none;
  }
`;
