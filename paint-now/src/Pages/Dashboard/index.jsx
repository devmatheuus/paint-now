import FormContainer from '../../Components/FormContainer/FormContainer';
import Header from '../../Components/Header';
import Main from '../../Components/Main/Main';

const Dashboard = () => {
  return (
    <>
      <Header />
      <Main>
        <FormContainer />
      </Main>
    </>
  );
};

export default Dashboard;
