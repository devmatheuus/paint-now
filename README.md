# Digital Republic - Code Challenge 

## Paint Now

Desenvolvido por: <a href="https://www.linkedin.com/in/devmatheuus/" target="_blank">Matheus Lima</a> 
Deploy: <a href="https://paint-now.vercel.app" target="_blank"> Acesse o projeto </a>

## Apresentando o projeto

O Paint Now é uma aplicação web desenvolvida para ajudar o usuário a evitar desperdícios na hora de pintar paredes, ele calcula a quantidade aproximada de tinta necessária levando em consideração alguns fatores:
 
 - O ambiente contém 4 paredes
  
 - Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes
 
 - O total de área das portas e janelas deve ser no máximo 50% da área de parede
 
 - A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
 
 - Cada janela possui as medidas: 2,00 x 1,20 mtos
 
 - Cada porta possui as medidas: 0,80 x 1,90
 
 - Cada litro de tinta é capaz de pintar 5 metros quadrados
 
 - Não considera teto nem piso.
 
 - As variações de tamanho das latas de tinta são:
    * 0,5 L
    * 2,5 L
    * 3,6 L
    * 18 L    


## Pré Requisitos

  * Node.js v16.15.0
  * Yarn v1.22.18

## Passo a Passo - Instalando a aplicação
 
  Para clonar o projeto, escolha um dos seguintes links e cole-o no terminal, no exemplo estou usando o git bash:

  SSH

  ```
  git clone git@gitlab.com:devmatheuus/paint-now.git
  ```

  HTTPS

  ```
  git clone https://gitlab.com/devmatheuus/paint-now.git
  ```

  Após fazer o clone do projeto, entre na pasta da aplicação com o comando:

  ```
    cd paint-now/pain-now
  ```

  Para instalar as dependências do projeto:

  ```
  yarn
  ```

  E por fim, rode o seguinte comando para rodar a aplicação localmente:

  ```
  yarn dev
  ```
